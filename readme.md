## Requirements
- [Docker](https://docs.docker.com/install)
- [Docker Compose](https://docs.docker.com/compose/install)

## Setup
1. Clone the repository.
1. Create a `.env` file and copy the contents of `.env-dev` and add your database connection credentials to the respective variables.
1. Update also the `.env` file inside the src directory and use the same DB credentials used in the docker `.env` file.
1. Once done, build the laravel container using this command: `docker-compose build laravel`.
1. Start the containers by running `docker-compose up -d` in the project root.
1. Install the composer packages by running `docker-compose exec laravel composer install`.
1. Access the Laravel instance on `http://localhost` (If there is a "Permission denied" error, run `docker-compose exec laravel chown -R www-data storage`).

Note that the changes you make to local files will be automatically reflected in the container. 

## Persistent database
If you want to make sure that the data in the database persists even if the database container is deleted, add a file named `docker-compose.override.yml` in the project root with the following contents.
```
version: "3.7"

services:
  mysql:
    volumes:
    - mysql:/var/lib/mysql

volumes:
  mysql:
```
Then run the following.
```
docker-compose stop \
  && docker-compose rm -f mysql \
  && docker-compose up -d
``` 

## Stopping containers
Stop and remove the containers generate by docker compose
`docker-compose down -v`

## Addtional Setup on development
Run the following upon starting the containers
```
docker-compose exec laravel composer install
docker-compose exec laravel npm install
```

Execute laravel migration command upon running container.
```
docker-compose exec laravel php artisan migrate
```

For compiling assets such as javascript files and CSS files, run the watch command 
to automatically compile asset files on change.
```
docker-compose exec -it laravel bash

npm run watch-poll
```

