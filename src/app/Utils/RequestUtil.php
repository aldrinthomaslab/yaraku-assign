<?php

namespace App\Utils;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

class RequestUtil
{
    /**
     * 
     */
    public static function getParamsOrDefault($reqQueries, $config = [])
    {
        $extractedParams = [];

        if (!count($config)) {
            return $extractedParams;
        }

        $validator = Validator::make($reqQueries, self::getValidationRules($config));
        $invalidFields = $validator->invalid();

        return self::getValueOrDefault($reqQueries, $invalidFields, $config);
    }

    /**
     * 
     */
    private static function getValidationRules($config = [])
    {   
        $rulesMap = [];

        if (!count($config)) {
            return $rulesMap;
        }

        foreach($config as $param => $paramConfig) {
            $rulesMap[$param] = $paramConfig['rules']; 
        }

        return $rulesMap;
    }

    /**
     * 
     */
    private static function getValueOrDefault($rawQueries, $invalidResult, $config)
    {
        $queries = [];

        if (!count($config)) {
            return $queries;
        }

        foreach($config as $paramKey => $paramConfig) {
            if (
                !array_key_exists($paramKey, $rawQueries) || 
                array_key_exists($paramKey, $invalidResult)
            ) {
                $queries[$paramKey] = $paramConfig['defaultVal'];
                continue;
            }

            $queries[$paramKey] = $rawQueries[$paramKey];
        }

        return $queries;
    }
}