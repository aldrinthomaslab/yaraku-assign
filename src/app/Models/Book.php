<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public static function getPaginated($queryParams) {
        $bookQuery = self::select(['id', 'title', 'author']);
        $appendParams = [
            'limit'  => $queryParams['limit'],
            'sortBy' => $queryParams['sortBy'],
            'order'  => $queryParams['order'],
            'page'   => $queryParams['page']
        ];
        
        if (array_key_exists('search', $queryParams) && $queryParams['search']) {
            $searchParam = '%' . $queryParams['search'] . '%';

            $bookQuery->where('title', 'like', $searchParam)
                    ->orWhere('author', 'like', $searchParam);
            
            $appendParams['search'] = $queryParams['search'];
        }

        return $bookQuery->orderBy($queryParams['sortBy'], $queryParams['order'])
                        ->paginate($queryParams['limit'])
                        ->appends($appendParams);
    }
}
