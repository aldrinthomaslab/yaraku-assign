<?php

namespace App\Http\Controllers;

use League\Csv\Writer;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

use App\Models\Book;
use App\Utils\RequestUtil;

class BookController extends Controller
{
    /**
     * Get books in paginated format.
     */
    public function getPage(Request $request) 
    {
        $reqConfig = [
            'sortBy' => [
                'rules'      => [ Rule::in(['title', 'author']), 'nullable' ],
                'defaultVal' => 'title'
            ],
            'order'  => [
                'rules'      => [ Rule::in(['desc', 'asc', 'ASC', 'DESC']), 'nullable' ],
                'defaultVal' => 'desc'
            ],
            'limit'  => [
                'rules'      => 'numeric|nullable',
                'defaultVal' => 10
            ],
            'page'   => [
                'rules'      => 'numeric|nullable',
                'defaultVal' => 1
            ]
        ];

        $queryParams = RequestUtil::getParamsOrDefault($request->query(), $reqConfig);

        if ($request->query('search')) {
            $queryParams['search'] = $request->query('search');
        }

        $books = Book::getPaginated($queryParams);
        $tableData = $books->map(function($book) {
            return Arr::flatten($book->toArray());
        });

        $headers = [
            [ 
                'name'     => 'id',
                'sortable' => false,
                'sorted'   => false // cannot sort by this column
            ],
            [ 
                'name' => 'Title',
                'sortable' => true,
                'sorted'   => ($queryParams['sortBy'] === 'title'),
                'order'    => $queryParams['order'],
                'url'      => $this->getSortUrl($books, 'title', $queryParams)
            ],
            [ 
                'name' => 'Author',
                'sortable' => true,
                'sorted'   => ($queryParams['sortBy'] === 'author'),
                'order'    => $queryParams['order'],
                'url'      => $this->getSortUrl($books, 'author', $queryParams)
            ]
        ];

        return view('books', [
            'books' => [
                'headers' => $headers,
                'data'    => $tableData->all(),
                'config'  => [
                    'idIndex' => 0,
                    'edit'    => [
                        // ID
                        [
                            'asInput' => false
                        ],
                        // title
                        [
                            'asInput' => true,
                            'inputIdPrefix' => 'book-data-title' 
                        ],
                        // author
                        [
                            'asInput' => true,
                            'inputIdPrefix' => 'book-data-author' 
                        ],
                    ]
                ]
            ],
            'pagination'  => $books,
            'query'       => $queryParams
        ]);
    }

    /**
     * Create new book record.
     */
    public function create(Request $request) 
    {
        $returnConfig = [
            'request' => 'create'
        ];

        $bookInput = $request->only([ 'title', 'author' ]);

        $validator = Validator::make(
            $bookInput, 
            [
                'title'  => 'required|string',
                'author' => 'required|string'
            ]
        );

        if ($validator->fails()) 
        {
            return redirect('/books')->withErrors($validator)
                                ->withInput()
                                ->with($returnConfig);
        }
        
        try
        {
            $book = new Book;
        
            $book->title  = $bookInput['title'];
            $book->author = $bookInput['author'];
            $book->save();
        } catch (Exception $error)
        {
            $returnConfig['reqError'] = true;
            $returnConfig['reqErrorType'] = 'system';

            return redirect('/books')->withErrors($validator)
                                ->withInput()
                                ->with($returnConfig);
        }
        
        $returnConfig['status'] = 'success';

        return redirect('/books')->with($returnConfig);
    }

    /**
     * Delete a book record.
     */
    public function delete(Request $request)
    {
        $returnConfig = [
            'request' => 'delete'
        ];

        $deleteReqData = $request->only([ 'deleteId' ]);

        $validator = Validator::make(
            $deleteReqData, 
            [ 'deleteId'  => 'required|numeric' ]
        );

        if ($validator->fails()) 
        {
            return redirect('/books')->withErrors($validator)
                                    ->with($returnConfig);
        }

        $targetBook = Book::where('id', $deleteReqData['deleteId'])->first();

        if (!$targetBook)
        {
            $returnConfig['reqError'] = true;
            $returnConfig['reqErrorType'] = 'nonexistent';
            return redirect('/books')->with($returnConfig);
        }

        try
        {
            $targetBook->delete();
        } catch (Exception $error)
        {
            $returnConfig['reqError'] = true;
            $returnConfig['reqErrorType'] = 'system';

            return redirect('/books')->withErrors($validator)
                                    ->with($returnConfig);;
        }
        
        $returnConfig['status'] = 'success';

        return redirect('/books')->with($returnConfig);
    }

    /**
     * Edit target book entry.
     */
    public function edit(Request $request)
    {
        $returnConfig = [
            'request' => 'edit'
        ];

        $editReqData = $request->only([ 'author', 'bookId' ]);

        $validator = Validator::make(
            $editReqData, 
            [ 
                'bookId'  => 'required|numeric',
                'author'  => 'required|string'
            ]
        );

        if ($validator->fails()) 
        {
            return redirect('/books')->withErrors($validator)
                                    ->with($returnConfig);
        }

        $targetBook = Book::where('id', $editReqData['bookId'])->first();

        if (!$targetBook)
        {
            $returnConfig['reqError'] = true;
            $returnConfig['reqErrorType'] = 'nonexistent';

            return redirect('/books')->with($returnConfig);
        }

        try
        {
            $targetBook->author = $editReqData['author'];
            $targetBook->save();
        } catch (Exception $error)
        {   
            $returnConfig['reqError'] = true;
            $returnConfig['reqErrorType'] = 'system';

            return redirect('/books')->withErrors($validator)
                                    ->with($returnConfig);
        }

        $returnConfig['status'] = 'success';

        return redirect('/books')->with($returnConfig);
    }

    public function export(Request $request)
    {
        $reqData = $request->only(['format', 'columns']);
        $allowedColumns = [ 'title', 'author' ];

        $targetColumns = array_intersect($allowedColumns, $reqData['columns']);
        $books = [];

        if (count($targetColumns))
        {
            $bookModels = Book::select($reqData['columns'])->get();
        }

        // Export data to CSV.
        if ($reqData['format'] === 'csv') {
            $books = $bookModels->map(
                function($book) use ($targetColumns) 
                {
                    return array_map(
                        function($column) use ($book)
                        {
                            return $book[$column];
                        }, 
                        $targetColumns
                    );
                }
            );
                
            return response()->streamDownload(
                function () use ($books, $targetColumns) 
                {
                    $csv = Writer::createFromPath('php://temp', 'r+');
                
                    $csv->insertOne($targetColumns);
                    $csv->insertAll($books);
                                    
                    // Output as csv. 
                    echo $csv->getContent();
            
                    flush();
                }, 
                'books-export-' . strtotime('now') . '.csv'
            );
        }
        
        // Export data to XML by default.
        return response()->streamDownload(
            function () use ($bookModels) 
            {
                echo collect([
                    'book' => $bookModels
                ])->toXml('books');
            }, 
            'books-export-' . strtotime('now') . '.xml'
        );
    }

    /**
     * Get the sort URL based on the current sort related parameters.
     */
    private function getSortUrl($pagination, $columnName, $queryParams) 
    {
        $urlParams = $queryParams;
        $urlParams['sortBy'] = $columnName;

        if ($queryParams['sortBy'] === $columnName && $queryParams['order'] === 'desc') 
        {
            $urlParams['order'] = 'asc';
        }

        if (
            $queryParams['sortBy'] === $columnName && $queryParams['order'] === 'asc' || 
            $queryParams['sortBy'] != $urlParams['sortBy']
        ) 
        {
            $urlParams['order'] = 'desc';
        }

        return $pagination->path() . '?' . http_build_query($urlParams);
    }
}
