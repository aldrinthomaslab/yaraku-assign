<?php
use App\Http\Controllers\BookController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('/books');
});

Route::get('/books', [ BookController::class, 'getPage' ]);
Route::post('/book', [ BookController::class, 'create' ]);
Route::delete('/book', [ BookController::class, 'delete' ]);
Route::patch('/book', [ BookController::class, 'edit' ]);
Route::get('/books/export', [ BookController::class, 'export' ]);
