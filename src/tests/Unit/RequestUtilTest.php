<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Validator;

use App\Utils\RequestUtil;

use Mockery;

class RequestUtilTest extends TestCase
{
    public function testItShouldReturnDefaultQueryParameters()
    {
        $queries = [];
        $mockConfig = [
            'param1' => [
                'rules' => 'nullable',
                'defaultVal' => 'default1' 
            ],
            'param2' => [
                'rules' => 'nullable',
                'defaultVal' => 'default2'
            ]
        ];

        Validator::shouldReceive('make')
                ->once()
                ->with($queries, [ 'param1' => 'nullable', 'param2' => 'nullable' ])
                ->andReturn(Mockery::mock([ 'invalid' => [] ]));

        $expected = [
            'param1' => 'default1',
            'param2' => 'default2',
        ];

        $result = RequestUtil::getParamsOrDefault($queries, $mockConfig);

        $this->assertArrayHasKey('param1', $result);
        $this->assertEquals($result['param1'], $mockConfig['param1']['defaultVal']);
        $this->assertArrayHasKey('param2', $result);
        $this->assertEquals($result['param2'], $mockConfig['param2']['defaultVal']);
    }

    public function testItShouldReturnExplicitQueryAndDefaultParameters()
    {
        $queries = [ 'param1' => 'sampleValue' ];
        $mockConfig = [
            'param1' => [
                'rules' => 'nullable',
                'defaultVal' => 'default1' 
            ],
            'param2' => [
                'rules' => 'nullable',
                'defaultVal' => 'default2'
            ]
        ];

        Validator::shouldReceive('make')
                ->once()
                ->with($queries, [ 'param1' => 'nullable', 'param2' => 'nullable' ])
                ->andReturn(Mockery::mock([ 'invalid' => [] ]));

        $expected = [
            'param1' => 'default1',
            'param2' => 'default2',
        ];

        $result = RequestUtil::getParamsOrDefault($queries, $mockConfig);

        $this->assertArrayHasKey('param1', $result);
        $this->assertEquals($result['param1'], $queries['param1']);
        $this->assertArrayHasKey('param2', $result);
        $this->assertEquals($result['param2'], $mockConfig['param2']['defaultVal']);
    }

    public function testItShouldReturnAllExplicitQueries()
    {
        $queries = [ 'param1' => 'sample value 1', 'param2' => 'sample value 2' ];
        $mockConfig = [
            'param1' => [
                'rules' => 'nullable',
                'defaultVal' => 'default1' 
            ],
            'param2' => [
                'rules' => 'nullable',
                'defaultVal' => 'default2'
            ]
        ];

        Validator::shouldReceive('make')
                ->once()
                ->with($queries, [ 'param1' => 'nullable', 'param2' => 'nullable' ])
                ->andReturn(Mockery::mock([ 'invalid' => [] ]));

        $expected = [
            'param1' => 'default1',
            'param2' => 'default2',
        ];

        $result = RequestUtil::getParamsOrDefault($queries, $mockConfig);

        $this->assertArrayHasKey('param1', $result);
        $this->assertEquals($result['param1'], $queries['param1']);
        $this->assertArrayHasKey('param2', $result);
        $this->assertEquals($result['param2'], $queries['param2']);
    }

    public function testItShouldReturnReplaceInvalidParameterValuesWithDefault()
    {
        $queries = [ 'param1' => 'sample value 1', 'param2' => 'sample value 2' ];
        $mockConfig = [
            'param1' => [
                'rules' => 'numeric|nullable',
                'defaultVal' => 1
            ],
            'param2' => [
                'rules' => 'nullable',
                'defaultVal' => 'default2'
            ]
        ];

        Validator::shouldReceive('make')
                ->once()
                ->with($queries, [ 'param1' => 'numeric|nullable', 'param2' => 'nullable' ])
                ->andReturn(Mockery::mock([ 'invalid' => [ 'param1' => 'sample value 1' ] ]));

        $expected = [
            'param1' => 'default1',
            'param2' => 'default2',
        ];

        $result = RequestUtil::getParamsOrDefault($queries, $mockConfig);

        $this->assertArrayHasKey('param1', $result);
        $this->assertEquals($result['param1'], $mockConfig['param1']['defaultVal']);
        $this->assertArrayHasKey('param2', $result);
        $this->assertEquals($result['param2'], $queries['param2']);
    }
}
