@extends('layouts.app')

@section ('content')
    <div class="container books-page">
        <!-- Section:  title header-->
        <div class="row books-page__header">
            <div class="col">
                <h1 class="text-primary">
                    <a href="/books">Books</a>
                </h1>
            </div>
        </div>
        
        <div class="row">
            <div class="col">
                @component('components.bookAlerts', [ 'errors' => $errors ])
                @endcomponent
            </div>
        </div>

        <!-- Section: Book page header controls -->
        <div class="row books-page__header-ctrls">
            <div class="col">
                <button  class="btn btn-primary mr-3" data-toggle="modal" data-target="#bookFormModal">
                    <i class="fa-regular fa-square-plus"></i> Add Book
                </button>
                @if(count($books['data']))
                    <button  class="btn btn-primary" data-toggle="modal" data-target="#bookExportModal">
                        <i class="fa-solid fa-file-arrow-down"></i> Export
                    </button>
                @endif
            </div>
            <!-- Section: Search books component -->
            <div class="col d-flex justify-content-end">
                @component('components.bookSearchForm')
                @endcomponent
            </div>
        </div>

        <!-- Section: Book page table info -->
        <div class="row">
            <div class="col">
                @if ($pagination->currentPage() <= $pagination->lastPage())
                    <h5 class="text-white">
                        Page {{ $pagination->currentPage() }} of {{ $pagination->lastPage() }}
                    </h5>
                @else
                    <h5 class="text-white">
                        Page number exceeded maximum page number: {{ $pagination->lastPage() }}
                    </h5>
                @endif
            </div>
            <div class="col">
                <h5 class="text-right text-white">
                    Total items: {{ $pagination->total() }}
                </h5>
            </div>
        </div>
        
        @component('components.table', [ 'tableData' => $books])
        @endcomponent
        @component('components.paginationControl', [ 'pagination' => $pagination, 'query' => $query])
        @endcomponent
        @component('components.bookFormModal')
        @endcomponent
        @component('components.bookEditFormModal')
        @endcomponent
        @component('components.exportBooksModal')
        @endcomponent
    </div>
@endsection