<div class="modal book-form-modal book-export-modal" id="bookExportModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Export Books Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="GET" action="/books/export">
            @csrf
            <div class="form-group">
                <label for="exportFormatField">
                    Export file format
                </label>
                <select 
                    class="form-control" 
                    id="exportFormatField"
                    name="format"
                >
                    <option value="csv">CSV</option>
                    <option value="xml">XML</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exportFormatField">
                    Fields
                </label>
                <div class="form-check book-export-modal__checkbox-wrapper--all">
                    <input 
                        class="form-check-input book-export-modal__checkbox--all" 
                        type="checkbox" 
                        value="title" 
                        id="AllColumns"
                        checked
                    >
                    <label 
                        class="form-check-label mr-3" 
                        for="AllColumns"
                    >
                        All
                    </label>
                    <small  class="text-muted">
                        Uncheck to select specific columns to export.
                    </small>
                </div>
                <div class="form-check book-export-modal__checkbox-wrapper--field d-none">
                    <input 
                        class="form-check-input book-export-modal__checkbox--field" 
                        type="checkbox" 
                        value="title" 
                        id="titleColumn"
                        name="columns[]"
                        checked
                    >
                    <label 
                        class="form-check-label" 
                        for="titleColumn"
                    >
                        Title
                    </label>
                </div>
                <div class="form-check book-export-modal__checkbox-wrapper--field d-none">
                    <input 
                        class="form-check-input book-export-modal__checkbox--field" 
                        type="checkbox" 
                        value="author" 
                        id="authorColumn"
                        name="columns[]"
                        checked
                    >
                    <label 
                        class="form-check-label" 
                        for="authorColumn"
                    >
                        Author
                    </label>
                </div>
            </div>

            <div class="d-flex justify-content-end book-form-modal__footer-ctrl">
                <button 
                    type="submit" 
                    class="btn btn-primary book-form-modal__submit-btn"
                >
                    Export
                </button>
                <button 
                    type="button" 
                    class="btn btn-secondary book-form-modal__close-btn" 
                    data-dismiss="modal"
                >
                    Close
                </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
