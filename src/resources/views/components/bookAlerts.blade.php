<!-- Book record create: Error/Failed -->
@if (session('request') === 'create' && (($errors && $errors->isNotEmpty()) || session('reqError')))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div>
            @if(!session('reqError'))
                <p class="mb-0">Failed to create new book record.</p>
                <ul>
                    @foreach($errors->all() as $errMsg)
                        <li>{{ $errMsg }}</li>
                    @endforeach   
                </ul>
            @else
                <p class="mb-0">
                    Failed to create new book record. Please reload the page and try again. If the problem persists, please contact the administrator.
                </p>
            @endif
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Book record create: Success -->
@if(session('request') === 'create' && session('status') === 'success')
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <div>
            <p class="mb-0">Book record succesfully added!</p>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Book record edit: Error/Failed -->
@if (session('request') === 'edit' && (($errors && $errors->isNotEmpty()) || session('reqError')))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div>
            <p class="mb-0">
                @if(session('reqError') && session('reqErrorType') == 'nonexistent')
                    Failed to edit non-existent book record.
                @elseif(session('reqError') && session('reqErrorType') == 'general')
                    Failed to edit book record. Please reload the page and try again. If the problem persists, please contact the administrator.
                @else
                    Failed to edit book record.
                @endif
            </p>

            @if(!session('reqError'))
                <ul>
                    @foreach($errors->all() as $errMsg)
                        <li>{{ $errMsg }}</li>
                    @endforeach   
                </ul>
            @endif
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Book record edit: Success -->
@if(session('request') === 'edit' && session('status') === 'success')
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <div>
            <p class="mb-0">Book record succesfully edited!</p>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Book record delete: Error/Failed -->
@if (session('request') === 'delete' && (($errors && $errors->isNotEmpty()) || session('reqError')))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div>
            <p class="mb-0">
                @if(session('reqError') && session('reqErrorType') == 'nonexistent')
                    Failed to delete non-existent book record.
                @elseif(session('reqError') && session('reqErrorType') == 'general')
                    Failed to delete book record. Please reload the page and try again. If the problem persists, please contact the administrator.
                @else
                    Failed to edit book record.
                @endif
            </p>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<!-- Book record delete: Success -->
@if(session('request') === 'delete' && session('status') === 'success')
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <div>
            <p class="mb-0">Book record succesfully deleted!</p>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif