<div class="row d-flex align-items-center">
        <div class="col d-flex flex-row">
            @if ($pagination->currentPage() != 1)
                <a role="button" class="btn btn-primary mr-2" href="{{ $pagination->previousPageUrl() }}">
                    Previous
                </a>
            @endif
            <div class="dropdown mr-2">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                    {{ $pagination->currentPage() }}
                </button>
                <ul class="dropdown-menu">
                    @for ($pageNumber = 1; $pageNumber <= $pagination->lastPage(); $pageNumber++)
                        <li>
                            <a class="dropdown-item" href="{{ $pagination->url($pageNumber) }}">{{ $pageNumber }}</a>
                        </li>
                    @endfor
                </ul>
            </div>
            @if($pagination->currentPage() != $pagination->lastPage())
                <a role="button" class="btn btn-primary" href="{{ $pagination->nextPageUrl() }}">
                    Next
                </a>
            @endif
        </div>
        <div class="col d-flex flex-row align-items-center justify-content-end">
            <h6 class="mb-0 mr-2 text-white">Items per page: </h6> 
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                    {{ $pagination->perPage() }}
                </button>
                <ul class="dropdown-menu">
                    <li>
                        @foreach([10, 15, 20, 25] as $itemCount)
                            <a class="dropdown-item" href="{{ $pagination->appends('limit', $itemCount)->url($pagination->currentPage()) }}">
                                {{ $itemCount }}
                            </a>
                        @endforeach
                    </li>
                </ul>
             </div>
        </div>
    </div>