<form class="form-inline book-search-form" action="books" method="GET">
    <input 
        type="text"
        id="search"
        name="search"
        class="form-control book-search-form__input" 
        placeholder="Search by title or author"
    />
    <button class="btn btn-primary" type="submit">
        <i class="fa-solid fa-magnifying-glass"></i>
    </button>
</form>