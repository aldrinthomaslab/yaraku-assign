<div class="modal book-form-modal book-form-edit-modal" id="bookEditFormModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Book Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="/book">
            @csrf
            @method('PATCH')
            <input 
                id="bookId"
                name="bookId"
                type="hidden" 
                class="form-control book-form-edit-modal__input-id" 
                value=""  
            />
            <div class="form-group">
                <label for="title">Title</label>
                <input 
                    disabled                  
                    required 
                    id="title"
                    name="title"
                    type="text" 
                    class="form-control book-form-edit-modal__input-title {{ (session('request') === 'edit' && $errors->has('title')) ? 'is-invalid':'' }}" 
                    value="{{ old('title') }}"  
                />
            </div>
            <div class="form-group">
                <label for="author">
                  Author<span class="text-danger">*</span>
                </label>
                <input     
                    required 
                    id="author"
                    name="author"
                    type="text" 
                    class="form-control book-form-edit-modal__input-author {{ (session('request') === 'edit' && $errors->has('author')) ? 'is-invalid':'' }}" 
                    value="{{ old('author') }}" 
                />
                @if (session('request') === 'edit' && $errors->has('author'))
                  <div id="authorFeedback" class="invalid-feedback">
                    {{ $errors->first('author') }}
                  </div>
                @endif
            </div>
            <div class="d-flex justify-content-end book-form-modal__footer-ctrl">
                <button 
                    type="submit" 
                    class="btn btn-primary book-form-modal__submit-btn"
                >
                    Save
                </button>
                <button 
                    type="button" 
                    class="btn btn-secondary book-form-modal__close-btn book-edit-form-modal__close-btn" 
                    data-dismiss="modal"
                >
                    Cancel
                </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
