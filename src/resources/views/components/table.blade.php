<div class="table-responsive">
    <table class="table table-dark table-striped app-table">
        <thead>
            <tr>
                <!-- <th scope="col" class="align-middle app-table__check-col">
                    <input 
                        class="form-control mx-auto app-table__checkbox" 
                        type="checkbox" 
                        value="all"
                    >
                </th> -->
                @foreach($tableData['headers'] as $header)
                    <th scope="col" class="align-middle">
                        {{ $header['name'] }}
                        @if ($header['sortable'])
                            <a href="{{ $header['url'] }}">
                                @if (!$header['sorted'])
                                    <i class="fa-solid fa-sort"></i>
                                @endif
                                
                                @if ($header['sorted'] && $header['order'] === 'desc')
                                    <i class="fa-solid fa-sort-down"></i>
                                @endif

                                @if ($header['sorted'] && $header['order'] === 'asc')
                                    <i class="fa-solid fa-sort-up"></i>
                                @endif
                            </a>
                        @endif
                    </th>
                @endforeach
                <th scope="col" class="align-middle app-table__action-col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tableData['data'] as $dataRow)
                <tr class="table__data-row-{{ $dataRow[$tableData['config']['idIndex']] }}">
                    <!-- <td class="align-middle">
                        <input 
                            class="form-control mx-auto app-table__checkbox" 
                            type="checkbox" 
                            value="{{ $dataRow[$tableData['config']['idIndex']] }}"
                        >
                    </td> -->
                    @foreach($dataRow as $dataIndex => $dataCol)
                        <td class="align-middle">
                            <span >{{ $dataCol }}</span>
                            @if (
                                $tableData['config']['edit'][$dataIndex] && 
                                $tableData['config']['edit'][$dataIndex]['asInput']
                            )
                                <input 
                                    type="hidden" 
                                    id="{{ $tableData['config']['edit'][$dataIndex]['inputIdPrefix'] }}-{{ $dataRow[$tableData['config']['idIndex']] }}"
                                    value="{{ $dataCol }}"
                                />
                            @endif
                            
                        </td>
                    @endforeach
                    <td class="align-middle">
                        <div class="table__row-action-{{ $dataRow[$tableData['config']['idIndex']] }}">
                            <button 
                                type="button" 
                                class="btn btn-outline-primary table__edit-btn"
                                data-toggle="modal"
                                data-target="#bookEditFormModal"
                                data-ref="{{ $dataRow[$tableData['config']['idIndex']] }}"
                            >
                                <i class="fa-regular fa-pen-to-square"></i>
                            </button>
                            <button 
                                type="button" 
                                class="btn btn-outline-secondary table__delete-btn" 
                                data-ref="{{ $dataRow[$tableData['config']['idIndex']] }}"
                            >
                                <i class="fa-regular fa-trash-can"></i>
                            </button>
                        </div>
                        <form 
                            class="d-none table__delete-form-{{ $dataRow[$tableData['config']['idIndex']] }}"
                            method="POST"
                            action="/book"
                        >
                            @csrf
                            @method('DELETE')
                            <input 
                                type="hidden" 
                                class="table__delete-input-{{ $dataRow[$tableData['config']['idIndex']] }}" 
                                id="deleteId" 
                                name="deleteId"
                                value="{{ $dataRow[$tableData['config']['idIndex']] }}"
                            />
                            <button 
                                type="submit" 
                                class="btn btn-outline-danger" 
                            >
                                <i class="fa-solid fa-check"></i>
                            </button>
                            <button 
                                type="button" 
                                class="btn btn-secondary table__delete-cancel"
                                data-ref="{{ $dataRow[$tableData['config']['idIndex']] }}"
                            >
                                <i class="fa-solid fa-xmark"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

            @empty($tableData['data'])
                <tr>
                    <td scope="row" colspan="{{ count($tableData['headers']) + 1 }}">
                        <h4 class="text-center">No data to display.</h4>
                    </td>
                </tr>
            @endempty
        </tbody>
    </table>
</div>