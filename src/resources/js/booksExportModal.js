$('.book-export-modal__checkbox--all').on('click', function (element) {
  toggleColumnsVisibility(element.target.checked);
});

function toggleColumnsVisibility(visible) {
  var checkboxWrappers = $('.book-export-modal__checkbox-wrapper--field');

  if (!visible) {
    checkboxWrappers.removeClass('d-none');
  } else {
    checkboxWrappers.addClass('d-none');
    $('.book-export-modal__checkbox--field').prop('checked', true);
  }
}