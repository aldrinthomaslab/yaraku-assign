$('.table__delete-btn').on('click', function (element) {
  var deleteDataRef= element.currentTarget.attributes['data-ref'].value;
  
  $('.table__data-row-' + deleteDataRef).addClass('table-danger');
  $('.table__row-action-' + deleteDataRef).addClass('d-none');
  $('.table__delete-form-' + deleteDataRef).removeClass('d-none');
});

$('.table__delete-cancel').on('click', function(element) {
  var deleteDataRef= element.currentTarget.attributes['data-ref'].value;
  
  $('.table__data-row-' + deleteDataRef).removeClass('table-danger');
  $('.table__row-action-' + deleteDataRef).removeClass('d-none');
  $('.table__delete-form-' + deleteDataRef).addClass('d-none');
});

$('.table__edit-btn').on('click', function(element) {
  var editDataRef= element.currentTarget.attributes['data-ref'].value;

  var title = $('#book-data-title' + '-' + editDataRef).val();
  var author = $('#book-data-author' + '-' + editDataRef).val();

  $('.book-form-edit-modal__input-title').val(title);
  $('.book-form-edit-modal__input-author').val(author);
  $('.book-form-edit-modal__input-id').val(editDataRef);
});

$('.book-edit-form-modal__close-btn').on('click', function() {
  $('.book-form-edit-modal__input-title').val(null);
  $('.book-form-edit-modal__input-author').val(null);
  $('.book-form-edit-modal__input-id').val(null);
});

